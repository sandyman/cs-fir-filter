﻿using System;
using System.Collections.Generic;

namespace Fir
{
	public class Program
	{
		/**
		 * Main entry point
		 */
		public static void Main (string[] args)
		{
			List<Double> coefs = new List<Double> ();
			coefs.Add (-0.041375828504686483);
			coefs.Add (0.06930490630867815);
			coefs.Add (0.28976592564120235);
			coefs.Add (0.41367721031304966);
			coefs.Add (0.28976592564120235);
			coefs.Add (0.06930490630867815);
			coefs.Add (-0.041375828504686483);

			Double cSum = 0.0;
			for (int i = 0; i < coefs.Count; ++i)
				cSum += coefs [i];

//			FirFilter fir = new FirFilter (coefs);
			FirFilterLinearPhase lpfir = new FirFilterLinearPhase (coefs);

			Double sum = 0.0;
			int n_taps = coefs.Count;
			for (int i = 0; i < coefs.Count; ++i) {
				Double sample = i >= 0 ? 1.0 : 0.0;
//				Double y = fir.Add (sample);
				Double lpy = lpfir.Add (sample);
//				Console.WriteLine (y);
				Console.WriteLine (lpy);
				sum = lpy;
			}

			Console.WriteLine ("Sum of coefficients:  " + cSum);
			Console.WriteLine ("Sum of filter output: " + sum);
		}
	}
}

