﻿using System;
using System.Collections.Generic;

namespace Fir
{
	public class FirFilterLinearPhase : FirFilterBase
	{
		private int halfNrOfTaps;	// Half the number of taps

		/**
		 * Constructor
		 */
		public FirFilterLinearPhase (List<Double> coefficients) 
			: base(coefficients)
		{
			nrOfTaps = coefficients.Count;

			if (nrOfTaps == 0)
				throw new FirFilterException ();

			if ((nrOfTaps % 2) == 0)
				throw new FirFilterLinearPhaseException (nrOfTaps);

			halfNrOfTaps = nrOfTaps / 2;
			h = new List<Double> (halfNrOfTaps + 1);
			for (int i = 0; i < halfNrOfTaps + 1; ++i)
				h.Add(coefficients[i]);

			z = new List<Double> (2 * nrOfTaps);
			for (int i = 0; i < 2 * nrOfTaps; ++i)
				z.Add (0.0);
		}

		/**
		 * This is the kernel of the FIR in a sense
		 */
		protected override Double Calc()
		{
			Double sum = 0;

			int i = 0;
			while (i < halfNrOfTaps) {
				sum += h [i] * (z [zIndex + i] + z[zIndex + nrOfTaps - i - 1]);
				++i;
			}
			sum += h [i] * z [zIndex + i];

			if (--zIndex < 0)
				zIndex += nrOfTaps;

			return sum;
		}
	}
}
