﻿using System;
using System.Collections.Generic;

namespace Fir
{
	/**
	 * This implementation of a FIR filter uses a linearised circular buffer. 
	 */
	abstract public class FirFilterBase
	{
		protected int zIndex;			// Index into simulated circular buffer
		protected int nrOfTaps;			// Length of FIR filter - nr of taps
		protected List<Double> h;		// Coefficients
		protected List<Double> z;		// Buffer - holding twice the number of taps

		public FirFilterBase(List<Double> coefficients)
		{
			nrOfTaps = coefficients.Count;
			if (nrOfTaps == 0)
				throw new FirFilterException ();

			zIndex = 0;
			z = new List<Double> (2 * nrOfTaps);
			for (int i = 0; i < 2 * nrOfTaps; ++i)
				z.Add (0.0);
		}

		/**
		 * Clear FIR filter buffer, reset state
		 */
		public void Clear ()
		{
			zIndex = 0;
			for (int i = 0; i < nrOfTaps; ++i)
				z [i] = z[zIndex + i] = 0.0;
		}

		/**
		 * Add a sample x[n], calculate filter, return output y[n]
		 */
		public Double Add (Double sample)
		{
			z [zIndex] = z [zIndex + nrOfTaps] = sample;

			return Calc ();
		}

		abstract protected Double Calc ();
	}
}
	