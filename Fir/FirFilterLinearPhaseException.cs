﻿using System;

namespace Fir
{
	public class FirFilterLinearPhaseException : System.ApplicationException
	{
		public FirFilterLinearPhaseException (int value) : base(String.Format("Linear Phase FIR filter must have odd number of taps ({0} given)", value)) {} 
	}
}

