﻿using System;

namespace Fir
{
	public class FirFilterException : System.ApplicationException
	{
		public FirFilterException () : base("No coefficients provided for FIR filter") {} 
	}
}

