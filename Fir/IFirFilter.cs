﻿using System;

namespace Fir
{
	public interface IFirFilter
	{
		void Clear();

		Double Add(Double sample);
	}
}
