﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using Fir;

namespace Fir.Tests
{
	[TestFixture ()]
	public class Test
	{
		Double cSum;
		List<Double> coefficients;

		[SetUp()]
		public void SetUp()
		{
			coefficients = new List<Double> ();
			coefficients.Add (1.0 / 6.0);
			coefficients.Add (1.0 / 3.0);
			coefficients.Add (1.0 / 2.0);
			coefficients.Add (1.0 / 3.0);
			coefficients.Add (1.0 / 6.0);

			cSum = 0.0;
			for (int i = 0; i < coefficients.Count; ++i)
				cSum += coefficients [i];
		}

		[Test ()]
		public void WillThrowWhenEmptyCoefficients ()
		{
			Assert.Throws<FirFilterException> (delegate {
				new FirFilter (new List<Double> ());
			});
		}

		[Test ()]
		public void OutputAreCoefficientsAfterImpulseInput ()
		{
			FirFilter filter = new FirFilter (coefficients);

			for (int i = 0; i < coefficients.Count; ++i) {
				Double sample = (i == 0) ? 1.0 : 0.0;
				Double yn = filter.Add (sample);

				Assert.That (yn == coefficients [i]);
			}
		}

		[Test ()]
		public void LinearPhaseOutputAreCoefficientsAfterImpulseInput ()
		{
			FirFilterLinearPhase lpFilter = new FirFilterLinearPhase (coefficients);

			for (int i = 0; i < coefficients.Count; ++i) {
				Double sample = (i == 0) ? 1.0 : 0.0;
				Double yn = lpFilter.Add (sample);
				Assert.That (yn == coefficients [i]);
			}
		}

		[Test ()]
		public void LinearPhaseRequiresOddNumberOfCoefficients ()
		{
			// We start with an odd number of coefficients: no exception
			Assert.DoesNotThrow (delegate {
				new FirFilterLinearPhase (coefficients);
			});
		}

		[Test ()]
		public void LinearPhaseShouldThrowForEvenNumberOfCoefficients ()
		{
			// Add one, and we're at an even number: expect exception
			coefficients.Add (1.0 / 6.0);
			Assert.Throws<FirFilterLinearPhaseException> (delegate {
				new FirFilterLinearPhase (coefficients);
			});
		}

		[Test ()]
		public void OutputAreDCGainAfterOneInput ()
		{
			FirFilter filter = new FirFilter (coefficients);

			Double sum = 0.0;
			for (int i = 0; i < coefficients.Count; ++i) {
				Double yn = filter.Add (1.0);
				sum = yn;
			}
			Assert.That (sum == cSum);
		}

		[Test ()]
		public void LinearPhaseOutputAreDCGainAfterOneInput ()
		{
			FirFilterLinearPhase lpFilter = new FirFilterLinearPhase (coefficients);

			Double sum = 0.0;
			for (int i = 0; i < coefficients.Count; ++i) {
				sum = lpFilter.Add (1.0);
			}
			Assert.That (sum == cSum);
		}
	}
}

