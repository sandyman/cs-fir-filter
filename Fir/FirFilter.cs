﻿using System;
using System.Collections.Generic;

namespace Fir
{
	public class FirFilter : FirFilterBase
	{
		/**
		 * Constructor
		 */
		public FirFilter (List<Double> coefficients) 
			: base(coefficients)
		{
			h = coefficients;
		}

		/**
		 * This function performs the actual FIR calculations (kernel, if you will)
		 */
		protected override Double Calc ()
		{
			Double sum = 0;

			for (int i = 0; i < nrOfTaps; ++i) {
				sum += h [i] * z [zIndex + i];
			}

			if (--zIndex < 0)
				zIndex += nrOfTaps;

			return sum;
		}
	}
}
